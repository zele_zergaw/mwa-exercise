import { User } from './user';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  topic  = ['Angular','React','Vue'];

  userModel = new User('zelalem','zola@gmail.ocm',123456789,'');
}
